﻿

#include <iostream>
#include <string>
#include <queue>

int main()
{
    std::string salute = "Hello World";
    char firstChar{ salute[0] }, lastChar{ salute[salute.length() - 1] };


    std::cout << salute << "\n" << salute.length() << "\n" << firstChar << " " << lastChar << "\n\n";

    //Вывод первого и последнего символа строки методами front() и back()
    std::cout << salute.front() << " " << salute.back() << "\n";

}

